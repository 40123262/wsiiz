﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSceneLoaded : MonoBehaviour
{
    public GameObject controller;
    private void Awake()
    {
        if(GameController.instance==null)
        {
            Instantiate(controller);
        }
        GameController.instance.SpawnPlayer();
        GameController.instance.FadeInAnimation();

    }
}
