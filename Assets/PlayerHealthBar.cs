﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{

    public Image healthImage;
    // Start is called before the first frame update

    public void UpdateHealthBar()
    {
        healthImage.fillAmount = GameController.instance.player.stats.Health / GameController.instance.player.stats.MaxHealth;
    }
}
