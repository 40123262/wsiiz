﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;
        DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    public void LoadGame(string name)
    {
        StartCoroutine(LoadGameAsync(name));
    }

    private IEnumerator LoadGameAsync(string name)
    {

        PlayerData data = SaveSystem.LoadGame(name);       
        SceneManager.LoadSceneAsync(data.mapIndex);
        while(SceneManager.GetActiveScene().buildIndex!=data.mapIndex)
        {
            yield return null;
        }
        GameController.instance.SpawnPlayer();
        var player = GameController.instance.player;

        player.stats.Level = data.Level;
        player.stats.Name = data.Name;
        player.stats.Experience = data.Experience;
        player.SetPosition(new Vector3(data.posX, 0, data.posZ));


    }
    public void NewGame(string name)
    {
        SaveSystem.NewGame(name);
        StartCoroutine(LoadGameAsync(name));
    }
}
