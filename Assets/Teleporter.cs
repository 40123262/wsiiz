﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleporter : MonoBehaviour
{

    public int destination;
    public Vector3 position;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger entered!");

        StartCoroutine(WaitForFade());
    }
    IEnumerator WaitForFade()
    {
        GameController.instance.FadeOutAnimation();
        GameController.instance.SaveGameState();
        while (GameController.instance.IsFadeAnimationPlaying())
        {
            yield return null;
        }
        GameController.instance.SpawnPlayer();
        GameController.instance.player.SetPosition(position);
        SceneManager.LoadSceneAsync(destination);
        

    }

}
