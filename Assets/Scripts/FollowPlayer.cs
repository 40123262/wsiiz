﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    public Vector3 base_offset;
    private Vector3 offset;
    public float FollowSpeed = 2.0f;
    public float MaxZoomAmount = 0.6f;
    public float MinZoomAmount = 1.3f;


    private float ZoomAmount = 1;
    // Start is called before the first frame update
    void Awake()
    {

        StartCoroutine(WaitForPlayer());

    }
    private void Start()
    {
       
        if (PlayerPrefs.HasKey("Zoom"))
        {
            ZoomAmount = PlayerPrefs.GetFloat("Zoom");
        }
        offset = base_offset * ZoomAmount;
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            transform.position = Vector3.Lerp(transform.position, player.position + offset, Time.deltaTime * FollowSpeed);
        }

#if !(UNITY_ANDROID || UNITY_IOS)

        if(Input.mouseScrollDelta.magnitude>0)
        {
            ZoomAmount = Mathf.Clamp(ZoomAmount - Input.mouseScrollDelta.y * 0.03f, MaxZoomAmount, MinZoomAmount);
            offset = base_offset * ZoomAmount;
            PlayerPrefs.SetFloat("Zoom", ZoomAmount);

        }
#else
        
        if(Input.touchCount>=2)
        {
            Touch touch0, touch1;

            touch0 = Input.GetTouch(0);
            touch1 = Input.GetTouch(1);

            Vector2 touch0PrevPosition = touch0.position - touch0.deltaPosition;
            Vector2 touch1PrevPosition = touch1.position - touch1.deltaPosition;

            float prevMagnitude = (touch0PrevPosition - touch1PrevPosition).magnitude;
            float currMagniture = (touch0.position - touch1.position).magnitude;

            float difference = (currMagniture - prevMagnitude) * 0.001f;

            if(Mathf.Abs(difference) > 0.01f)
            {
                ZoomAmount = Mathf.Clamp(ZoomAmount - difference, MaxZoomAmount, MinZoomAmount);
                offset = base_offset * ZoomAmount;
                PlayerPrefs.SetFloat("Zoom", ZoomAmount);

            }

        }

#endif

    }

    IEnumerator WaitForPlayer()
    {
        while (player == null)
        {
            yield return null;
            var obj = GameObject.FindGameObjectWithTag("Player");
            if(obj!=null)
            {
                player = obj.transform;
                transform.position = player.position + offset;
            }

        }
        
        
    }
}
