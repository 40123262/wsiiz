﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGEnemyController : MonoBehaviour
{

    public RPGPlayerController target;
    public Rigidbody2D body;
    public float Speed = 5.0f;
    public bool isBlocked = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }
    public void SetTarget(RPGPlayerController t)
    {
        target = t;
    }
    public void ResetTarget()
    {
        target = null;
    }
    private void FollowPlayer()
    {
        if(target!=null && !isBlocked)
        {
            body.velocity= Vector3.Normalize(target.transform.position - transform.position) * Speed;
        }
        else if(target != null && isBlocked)
        {
            body.velocity = Vector3.right * Speed;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        isBlocked = true;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        isBlocked = false;
    }

}
