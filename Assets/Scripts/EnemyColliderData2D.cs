﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyColliderData2D : MonoBehaviour
{

    public RPGEnemyController controller;

    public bool isPlayerInReach = false;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            isPlayerInReach = true;
            controller.SetTarget(other.gameObject.GetComponent<RPGPlayerController>());
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            isPlayerInReach = false;
            controller.ResetTarget();
        }
    }
    
}
