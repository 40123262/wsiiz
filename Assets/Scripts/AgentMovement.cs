﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    public Animator anim;
    public AudioSource audio;
    private bool isMoving = false;

    private bool canMove = true;
    // Start is called before the first frame update
  
    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            anim.SetBool("isMoving", agent.hasPath);
            if(!agent.hasPath)
            {
                Debug.Log("Stopped walking");
                isMoving = false;
                if (audio != null)
                    audio.Stop();
            }
        }

    }

    public void MoveToPoint(Vector3 pos)
    {
        if (canMove)
        {
            isMoving = true;
            agent.SetDestination(pos);
            if (audio != null)
            {
                if(!audio.isPlaying)
                  audio.Play();
            }
        }
    }
    public void SetPosition(Vector3 pos)
    {
        agent.Warp(pos);
    }
    public void StopMoving()
    {
        agent.isStopped = true;
        agent.ResetPath();
    }
    public void Die()
    {
        canMove = false;
    }

}
