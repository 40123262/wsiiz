﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public PlayerHealthBar healhBar;
    public PlayerExperienceBar expBar;
    public Animation fadeAnim;

    [SerializeField]
    private GameObject PlayerPrefab;

    public PlayerController3D player;
    private void Awake()
    {
        if(instance!=null)
        {
            Destroy(gameObject);
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    public void UpdateHealthBar()
    {
        healhBar.UpdateHealthBar();
    }
    public bool IsFadeAnimationPlaying()
    {
        return fadeAnim.isPlaying;
    }
    public void FadeOutAnimation()
    {
        fadeAnim.Play("FadeOut");
    }
    public void SaveGameState()
    {
        SaveSystem.SaveGame(player);
    }

    public void AddExperience(int value)
    {
        player.stats.AddExperience(value);
        expBar.UpdateBar();
    }
    public void FadeInAnimation()
    {
        fadeAnim.Play("FadeIn");
    }
    // Start is called before the first frame update
    public void SpawnPlayer()
    {
        if (player == null)
        {
            var obj = GameObject.FindGameObjectWithTag("Player");
            if (obj != null)
            {
                player = obj.GetComponent<PlayerController3D>();
            }
            else
            {
                player = Instantiate(PlayerPrefab, Vector3.zero, Quaternion.identity).GetComponent<PlayerController3D>();
            }
        }
    }
    private void Start()
    {
#if UNITY_EDITOR
       // StartGame("Bob");
#endif
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            if (player != null)
            {
                Debug.Log("Position saved!");
                SaveSystem.SaveGame(player);
            }
            else
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController3D>();
                if (player != null)
                {
                    SaveSystem.SaveGame(player);
                    Debug.Log("Position saved!");
                }
                else
                    Debug.Log("ERROR");
            }
        }
    }

    private void SavePlayerPositon()
    {
        if (player != null)
        {
            Debug.Log("Position saved!");
            PlayerPrefs.SetFloat("PlayerX", player.transform.position.x);
            PlayerPrefs.SetFloat("PlayerZ", player.transform.position.z);
        }
    }
}
