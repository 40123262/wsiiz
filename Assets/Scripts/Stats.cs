﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public string Name = "player";
    public float Health = 100;
    public float MaxHealth = 100;
    public float Attack = 10.0f;
    public int Level = 1;
    public int Experience = 0;
    // Start is called before the first frame update
    


    public bool AddExperience(int value)
    {

        Experience += value;
        if(Experience>=Level*100)
        {
            Experience -= Level * 100;
            Level++;     
            return true;
        }
        return false;
    }
}
