﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGPlayerController : MonoBehaviour
{
    private Animator anim;
    Vector3 movement;
    public float Speed = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    void GetInput()
    {

        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");


    }
    void Animate()
    {
        if (movement.magnitude > 0)
        {
            anim.SetFloat("X", movement.x);
            anim.SetFloat("Y", movement.y);
            anim.SetFloat("Velocity", movement.magnitude);
        }
    }
    // Update is called once per frame
    void Update()
    {
        GetInput();
        Move();
        Animate();
    }
    void Move()
    {
        transform.position += movement * Time.deltaTime * Speed;
    }
}
