﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public AgentMovement movement;
    public PlayerController3D target;
    public bool isAttacking = false;
    public Animator anim;
    public EnemyColliderData colliderData;
    public Vector3 startingPos;
    public float RoamDelay = 5.0f;
    public float RoamDistanceX = 5.0f;
    public float RoamDistanceZ = 5.0f;
    private float RoamTimer = 0;
    public Stats stats;
    public bool isAlive = true;

    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
        RoamTimer = Random.Range(0, RoamDelay);
    }
    public void SetTarget(PlayerController3D t)
    {
        target = t;
    }
    public void ResetTarget()
    {
        target = null;
        
    }

    private void Roam()
    {
        RoamTimer -= Time.deltaTime;
        if(RoamTimer<=0.0f)
        {
            Debug.Log("New position");
            RoamTimer = RoamDelay;

            float newPosX = Random.Range(-RoamDistanceX, RoamDistanceX);
            float newPosZ = Random.Range(-RoamDistanceZ, RoamDistanceZ);

            Vector3 newPositon = new Vector3( newPosX, 0, newPosZ);


            movement.MoveToPoint(transform.position + newPositon);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (colliderData.IsPlayerInReach())
            {
                if(target!=null)
                { 
                float distance = Vector3.Distance(transform.position, target.transform.position);
                if (distance >= 8.0f)
                {
                    MoveToTarget();
                    StopAttacking();
                }
                else
                {
                    movement.StopMoving();
                    StartAttacking();
                }
                if (isAttacking)
                {
                    FaceTarget();
                }
                }
            }
            else
            {
                Roam();
            }
        }
    }
    private void FaceTarget()
    {
        Vector3 lookPos = target.transform.position - transform.position;
        lookPos.y = 0;

        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation,0.2f);

    }
    public void StartAttacking()
    {
        if (!isAttacking)
        {
            isAttacking = true;
            anim.SetBool("isAttacking", isAttacking);
        }
    }
    public void StopAttacking()
    {
        if (isAttacking)
        {
            isAttacking = false;
            anim.SetBool("isAttacking", isAttacking);

        }
    }
    public void MoveToTarget()
    {
        if(target!=null)
        {
            movement.MoveToPoint(target.transform.position);
        }
    }
    public void Hit()
    {
        if (target != null)
        {
            if(target.GetHit(stats.Attack))
            {
                target = null;
                StopAttacking();
            }
        }
    }
    public void Die()
    {
        isAlive = false;
        GameController.instance.AddExperience(stats.Experience);
        StopAttacking();
        GetComponent<CapsuleCollider>().enabled = false;
        anim.SetBool("isAlive", isAlive);
    }
    public bool GetHit(float value)
    {
        stats.Health -= value;
        if (stats.Health <= 0)
        {
            Die();
            return true;
        }
        return false;
    }
}
