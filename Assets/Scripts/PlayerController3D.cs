﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController3D : MonoBehaviour
{
    private Animator anim;
    private Vector3 destination;
    private GameObject target;
    public AgentMovement movement;
    public AudioSource audio;
    private bool isApproachingTarget = false;
    private bool isAttacking = false;
    public Stats stats;
    public bool isAlive = true;


    private void Awake()
    {
        DontDestroyOnLoad(this);

    }
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (Input.GetMouseButton(0))
            {
                Move();
                StopAttacking();
            }

            if (isApproachingTarget)
            {
                MoveToTarget();
            }
            if (isAttacking)
            {
                FaceTarget();
            }
        }
    }
    public void SetPosition(Vector3 pos)
    {
        movement.SetPosition(pos);
    }
    public void Move()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
        {
            destination = hit.point;
            if(hit.collider.transform.tag.Equals("Ground"))
            {
                movement.MoveToPoint(destination);
                isApproachingTarget = false;

            }
            else if (hit.collider.transform.tag.Equals("Enemy"))
            {
                target = hit.collider.gameObject;
                isApproachingTarget = true;
            }
        }
    }
    private void FaceTarget()
    {
        Vector3 lookPos = target.transform.position - transform.position;
        lookPos.y = 0;

        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.2f);

    }
    public void MoveToTarget()
    {
        if(target!=null)
        {
            float distance = Vector3.Distance(transform.position, target.transform.position);

            if(distance>=8.0f)
            {
                movement.MoveToPoint(target.transform.position);
               
            }
            else
            {
                movement.StopMoving();
                if(target.tag.Equals("Enemy"))
                {
                    StartAttacking();
                    isApproachingTarget = false;
                }
            }

        }
    }
    public void StartAttacking()
    {
        if (!isAttacking)
        {
            isAttacking = true;
            anim.SetBool("isAttacking", isAttacking);
        }
    }
    public void Hit()
    {
        if(target!=null)
        {
            EnemyController enemy = target.GetComponent<EnemyController>();
            if (enemy != null)
            {
                if(enemy.GetHit(stats.Attack))
                {
                    StopAttacking();
                    target = null;
                }
            }
        }
    }
    public void StopAttacking()
    {
        if (isAttacking)
        {
            isAttacking = false;
            anim.SetBool("isAttacking", isAttacking);

        }
    }
    public bool GetHit(float value)
    {
        stats.Health -= value;
        GameController.instance.UpdateHealthBar();
        audio.Play();
        if (stats.Health <= 0)
        {
            isAlive = false;
            StopAttacking();
            movement.Die();
            AudioManager.instance.Play("game_over");
            anim.SetBool("isAlive", isAlive);
            return true;
        }
        return false;
    }
}
