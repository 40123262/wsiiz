﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D body;
    private SpriteRenderer SpriteRenderer;
    public float speed;
    Vector2 movement;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody2D>();
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GetInput())
        {
            Move();
            Animate();
        }


        if(Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("Attack");
        }
    }

    bool GetInput()
    {

        movement.x = Input.GetAxis("Horizontal");
        if(movement.magnitude>0)
        {
            return true;
        }
        return false;
    }
    void Animate()
    {
        if(movement.x>0)
        {
            SpriteRenderer.flipX = false;
        }
        else if(movement.x<0)
        {
            SpriteRenderer.flipX = true;
        }
        anim.SetFloat("Velocity", movement.magnitude);
    }
    void Move()
    {
        body.velocity = movement * speed;
    }
}
