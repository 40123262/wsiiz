﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[Serializable]
public class PlayerData
{
    public int mapIndex;
    public float posX;
    public float posY;
    public float posZ;
    public int Level;
    public int Experience;
    public String Name;


    public PlayerData(PlayerController3D p)
    {
        mapIndex = SceneManager.GetActiveScene().buildIndex;
        posX = p.transform.position.x;
        posY = p.transform.position.y;
        posZ = p.transform.position.z;
        Name = p.stats.Name;
        Level = p.stats.Level;
        Experience = p.stats.Experience;
    }

    public PlayerData()
    {
        mapIndex = 1;
        posX = 0;
        posY = 0;
        posZ = 0;
        Name = "Player";
        Level = 1;
        Experience = 0;
    }
    public PlayerData(string name)
    {
        mapIndex = 1;
        posX = 0;
        posY = 0;
        posZ = 0;
        Name = name;
        Level = 1;
        Experience = 0;
    }

}
