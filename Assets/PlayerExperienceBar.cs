﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerExperienceBar : MonoBehaviour
{
    public Image expImage;
    // Start is called before the first frame update

    public void UpdateBar()
    {
        expImage.fillAmount = (float)GameController.instance.player.stats.Experience / (float)(GameController.instance.player.stats.Level * 100);
    }
}
